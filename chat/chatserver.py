#coding:utf8

#more about sockets https://docs.python.org/2/library/socket.html

# Echo server program
import socket, threading

HOST = '' # Symbolic name meaning all available interfaces
PORT = 9876  # Arbitrary non-privileged port
name=raw_input("Please enter your name: ")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
print("server started\nServer listening on host: " + HOST + " port: "+str(PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
print "\n\n\n"




def send_to_client():
    while True:
        ans=raw_input()
        if(ans == "exit"):
            conn.close()
            break
        conn.sendall(name+": " + str(ans))
        
def read_from_client():
    while True:
        data = conn.recv(1024)
        if not data: 
            break
        else:
            print(str(data))



try:
    t2 = threading.Thread(target=read_from_client)
    t2.start()
    send_to_client()

except:
    print "Error: unable to start thread"
