#coding:utf8


# Echo client program
import socket, threading

HOST = '31.208.70.137'    # The remote host
PORT = 9876         # The same port as used by the server
name=raw_input("Please enter your name: ")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

addlock = threading.Lock()

def send_to_server(lock=addlock):
    print("started sending to server\n")
    while True:
        ans=raw_input()
        if(ans == "exit"):
            s.close()
            break
        s.sendall(name + ": " + str(ans))
        
def read_from_server(lock=addlock):
    print("started reading from server\n")
    while True:
        data = s.recv(1024)
        if not data: 
            break
        else:
            print(str(data))
    

def main():
    try:

        t2 = threading.Thread(target=read_from_server)
        t2.start()
        send_to_server()

    except:
        print "Error: unable to start thread"
main()
